#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_matrix(int mat[5][5]);
void get_matrix_file(int mat[5][5]);
void set_matrix_file(int mat[5][5]);

int main() {
	int mat[5][5];
	get_matrix_file(mat);
	print_matrix(mat);
	mat[2][2] = 2;
	print_matrix(mat);
	set_matrix_file(mat);
	return (0);
}

void print_matrix(int mat[5][5]) {
	int i, j;
	printf("----------------------\n");
	for(i = 0; i < 5; i++) {
		for(j = 0; j < 5; j++) {
			printf("%d ", mat[i][j]);
		}
		printf("\n");
	}
	printf("----------------------\n");
}

void get_matrix_file(int mat[5][5]) {
	printf("Hello\n");
	FILE *fp;
	fp = fopen("messages.txt", "r");
	if (fp == NULL) {
		perror("fopen() failed"); exit(1);
	}
	char buffer[BUFSIZ];
	/* *************
	creating the game matrix
	* *************/
	int i = 0, j  = 0;
	const char *delim = ",";
	char *token;
	if ( fgets(buffer, BUFSIZ, fp) != NULL ) {
		token  = strtok(buffer, delim);
		while( token != NULL ) {
			
			mat[i][j] = atoi(token);
			j++;
			
			token = strtok(NULL, delim);
		}
		i++;
		j = 0;
	}	
	while( fgets(buffer, BUFSIZ, fp) != NULL ) {
		token  = strtok(buffer, delim);
		while( token != NULL ) {
			mat[i][j] = atoi(token);
			j++;
			token = strtok(NULL, delim);
		}
		i++;
		j = 0;
	}
	fclose(fp);
}

void set_matrix_file(int mat[5][5]) {
	FILE *fp = fopen("msgs.txt", "w+");
	int i, j;
	for ( i =0; i < 5; i++ ) {
		for (j = 0; j < 5; j++) {
			fputc(mat[i][j] + '0', fp);
			if(j == 4) fputc('\n', fp); else fputc(',', fp); 
		}
	}
	
	fclose(fp);
}
