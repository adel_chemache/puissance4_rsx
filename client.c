#if defined (WIN32)
    #include <winsock2.h>
    typedef int socklen_t;
#elif defined (linux)
	#include <time.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <string.h>
	#include <time.h>
    #include <unistd.h>
    #define INVALID_SOCKET -1
    #define SOCKET_ERROR -1
    #define closesocket(s) close(s)
    typedef int SOCKET;
    typedef struct sockaddr_in SOCKADDR_IN;
    typedef struct sockaddr SOCKADDR;
#endif
 
#include <stdio.h>
#include <stdlib.h>
#define PORT 23
 
void print_matrix(int mat[5][5]);

 
int main(void)
{

	//while(1){
    #if defined (WIN32)
        WSADATA WSAData;
        int erreur = WSAStartup(MAKEWORD(2,2), &WSAData);
    #else
        int erreur = 0;
    #endif
 
    SOCKET sock;
    SOCKADDR_IN sin;
 
    if(!erreur)
    {
        /* Création de la socket */
        sock = socket(AF_INET, SOCK_STREAM, 0);
 
        /* Configuration de la connexion */
        sin.sin_addr.s_addr = inet_addr("0.0.0.0");
        sin.sin_family = AF_INET;
        sin.sin_port = htons(5100);
 
        /* Si le client arrive à se connecter */
        //while(1){
        if(connect(sock, (SOCKADDR*)&sin, sizeof(sin)) != SOCKET_ERROR) {
            printf("Connexion à %s sur le port %d\n", inet_ntoa(sin.sin_addr), htons(sin.sin_port));
        	char message[BUFSIZ];
        	int adl = 0;
        	char ex[BUFSIZ];
        	srand(time(NULL));
        	
        	int player = 1 + rand() % (10 - 1);
        	/* **********
				init game matrix
			* **********/
			int mat[5][5];
			int mat2[5][5];
			int i = 0, j = 0;
			for (i = 0; i < 5; i++) {
        		for(j = 0; j < 5; j++) {
        			mat[i][j] = 0;
        		}
    		}
        	char *login = "adel_ch";
        	int column;
        	while(1) {
        		
        		//print_matrix(mat);
        		printf("In which column you want to play ? ... ");
        		scanf("%d", &column);
        		mat2[4][column] = player;
        		printf("your choice was (%d) result [%d] \n", column, mat[4][column]);
        		
        		
        		if(write(sock, mat2, sizeof(int) * 25) < 0) {
        			perror("send");
        		}
        		
        		//write(sock, mat, sizeof(mat));
        		//printf("Sent '%s'\n", ex);
        		
        		if(recv(sock, mat2, sizeof(int) * 25, 0) < 0) {
        			perror("receive");
        		}
        		print_matrix(mat2);
        		//print_matrix(mat2);
        		//printf("Le message recu : %s\n", message);
        		
        		
        		int c = getchar();
        		while ( c != '\n' && c != EOF) {
        			c = getchar();
        		}
        	}
        	
        }
        else
            printf("Impossible de se connecter\n");
        
 
        /* On ferme la socket précédemment ouverte */
        //int cc = getchar();
        closesocket(sock);
 		//}       
 
        #if defined (WIN32)
            WSACleanup();
        #endif
    }
 //}
    return EXIT_SUCCESS;
}

void print_matrix(int mat[5][5]) {
	//printf("\e[1;1H\e[2J");
	int i, j;
	printf("----------------------\n");
	for(i = 0; i < 5; i++) {
		for(j = 0; j < 5; j++) {
			printf("%d ", mat[i][j]);
		}
		printf("\n");
	}
	printf("----------------------\n");
}


