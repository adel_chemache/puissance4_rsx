#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <string.h>

void e(char *str);
void el(char *str);
void DieWithError(char *msg);
void handleTCPClient(int clientSocketid, char *messages[], int nb_msg);
void print_matrix(int mat[5][5]);
void get_matrix_file(int mat[5][5]);
void print_vector(int t[]);
void set_matrix_file(int mat[5][5]);
void update_file(int mat[5][5]);

int main () {
	e("\n************* server lunched ******************\n\n");
	// creating the server socket
	struct sockaddr_in addrport;
	struct sockaddr_in clientAddrport;	
	int socketid = socket(PF_INET, SOCK_STREAM, 0);
	int clientSocketid;
	int clientLen;
	unsigned int childProdCount = 0;
	char *messages[250];
	int nb_msg = 0;
	
	/*init game matrix (reseting it)*/
	int mat__[5][5], i__, j__;
	for(i__=0;i__<5;i__++) for(j__=0;j__<5;j__++) mat__[i__][j__] = 0;
	set_matrix_file(mat__);	
	addrport.sin_family = AF_INET;
	addrport.sin_port = htons(5100);
	addrport.sin_addr.s_addr = htonl(INADDR_ANY);		
	
	if ( (bind(socketid, (struct sockaddr *) &addrport, sizeof(addrport))) == -1 ) {
		perror("Error when binding");
	}
	printf("address %s:%d\n", inet_ntoa(addrport.sin_addr), 5100);
	
	if ( (listen(socketid, 5)) == -1 ) {
		perror("Listen");
	}
	
	int processID;
	for(;;) { // run forever ...
		clientLen = sizeof(clientAddrport);
		// wait for a client to connect
		if ( (clientSocketid = accept(socketid, (struct sockaddr *) &clientAddrport, &clientLen)) < 0) {
			DieWithError("accept() failed");
		}
		printf("Handeling client %s\n", inet_ntoa(clientAddrport.sin_addr));
		
		if ( (processID = fork()) < 0) DieWithError("fork() failed");
		else if (processID == 0) { // this the child process
			close(socketid); // child closes server socket
			handleTCPClient(clientSocketid, messages, nb_msg);
			exit(0);			
		}		
		
		close(clientSocketid);
		childProdCount++;
		
		while (childProdCount) {
			processID = waitpid( (pid_t) -1, NULL, WNOHANG);
			if (processID < 0) DieWithError("waitpid() failed");
			else if (processID == 0) break;
			else childProdCount--;
		}
		
			
	}
	
	

	// close the server socket
	int close_status;
	if ( (close_status = close(socketid)) == -1) {
		el("Error when closing socket");
	}
	
	
	
	return (0);	
}

void e(char *str) {
	printf("%s", str);
}
void el(char *str) {
	printf("%s\n", str);
}
void DieWithError(char *msg) {
	el(msg);
	exit(1);
}
void handleTCPClient(int clientSocketid, char *messages[], int nb_msg) {
	char buffer[BUFSIZ];//[5][5];
	int rcvMsgSize;
	/* **********
		init game matrix SERVER
	* **********/
	int mat[5][5];
	int mat2[5][5];
	int i = 0, j = 0;
	
	rcvMsgSize = recv(clientSocketid, mat2, sizeof(int) * 25, 0);
	get_matrix_file(mat2);
	int c;
	char *msg = "Hello";
	
	
	
	while (rcvMsgSize > 0) {
		update_file(mat2);
		//set_matrix_file(mat2);
		get_matrix_file(mat2);
		print_matrix(mat2);
		if(write(clientSocketid, mat2, sizeof(int) * 25) < 0) perror("write");
			
		if ( (rcvMsgSize = recv(clientSocketid, mat2, sizeof(int) * 25, 0)) < 0) {
			DieWithError("recv() failed");
		}
	}
	int closeClientSocket = close(clientSocketid);
}

void print_matrix(int mat[5][5]) {
	printf("\e[1;1H\e[2J");
	int i, j;
	printf("----------------------\n");
	for(i = 0; i < 5; i++) {
		for(j = 0; j < 5; j++) {
			printf("%d ", mat[i][j]);
		}
		printf("\n");
	}
	printf("----------------------\n");
}

void get_matrix_file(int mat[5][5]) {
	FILE *fp;
	fp = fopen("messages.txt", "r");
	if (fp == NULL) {
		perror("fopen() failed"); exit(1);
	}
	char buffer[BUFSIZ];
	/* *************
	creating the game matrix
	* *************/
	int i = 0, j  = 0;
	const char *delim = ",";
	char *token;
	if ( fgets(buffer, BUFSIZ, fp) != NULL ) {
		token  = strtok(buffer, delim);
		while( token != NULL ) {
			
			mat[i][j] = atoi(token);
			j++;
			
			token = strtok(NULL, delim);
		}
		i++;
		j = 0;
	}	
	while( fgets(buffer, BUFSIZ, fp) != NULL ) {
		token  = strtok(buffer, delim);
		while( token != NULL ) {
			mat[i][j] = atoi(token);
			j++;
			token = strtok(NULL, delim);
		}
		i++;
		j = 0;
	}
	fclose(fp);
}

void print_vector(int t[]) {
	int i;
	for(i = 0; i < 5; i++) {
		printf("%d", t[i]);
		if ( i == 4) printf("\n"); else printf(" ");
	}
}

void set_matrix_file(int mat[5][5]) {
	FILE *fp = fopen("messages.txt", "w+");
	int i, j;
	for ( i =0; i < 5; i++ ) {
		for (j = 0; j < 5; j++) {
			fputc(mat[i][j] + '0', fp);
			if(j == 4) fputc('\n', fp); else fputc(',', fp); 
		}
	}
	
	fclose(fp);
}

void update_file(int mat[5][5]) {
	int file_data[5][5];
	get_matrix_file(file_data);
	int i,j;
	for ( i = 0; i < 5; i++ ) {
		for ( j = 0; j < 5; j++ ) {
			if ( mat[i][j] != file_data[i][j]  && mat[i][j] != 0) {
				file_data[i][j] = mat[i][j];
			}
		}
	}
	set_matrix_file(file_data);
}
